package com.jwww.support.cache;

import com.jwww.support.spring.InstanceFactory;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * @author jwww
 * @date 2015年6月13日
 * @description <br>
 * Copyright (c) 2015, vakinge@gmail.com.
 */
public class CacheKit {

	private static CacheProvider provider;

	public static Object get(String key) {
		return getProvider().get(key);
	}

	public static void remove(String groupName, String key) {
		getProvider().remove(key);

		Set<String> keys = getChacheGroup(groupName);
		// 更新缓存租
		if (keys.contains(key)) {
			keys.remove(key);
			getProvider().set(groupName, keys, -1);
		}
	}

	public static void removeGroup(String groupName) {
		Set<String> keys = getChacheGroup(groupName);
		if (keys == null)
			return;
		getProvider().remove(keys);
	}
	
	public static void removeGroup(Collection<String> groupNames) {
		for (String g : groupNames) {
			removeGroup(g);
		}
	}

	public static void put(String group, String key, Object value, int timeout) {
		Set<String> keys = getChacheGroup(group);

		getProvider().set(key, value, timeout);
		// 更新缓存租
		if (!keys.contains(key)) {
			keys.add(key);
			//
			getProvider().set(group, keys, -1);
		}

	}
	
	public static void addGroupKey(String groupName, String key){
		Set<String> keys = getChacheGroup(groupName);
		if(keys.contains(key))return;
		keys.add(key);
		getProvider().set(groupName, keys, -1);
		
	}
	
    public static void removeGroupKey(String groupName, String key){
    	Set<String> keys = getProvider().get(groupName);
		if(keys == null || !keys.contains(key))return;
		keys.remove(key);
		if(keys.size() == 0){
			getProvider().remove(groupName);
		}else{
			getProvider().set(groupName, keys, -1);
		}
	}

	public static CacheProvider getProvider() {
		if (provider == null)
			provider = InstanceFactory.getInstance(CacheProvider.class);
		return provider;
	}

	private static Set<String> getChacheGroup(String groupName) {
		Set<String> groupKeys = getProvider().get(groupName);
		if (groupKeys == null)
			groupKeys = new HashSet<>();
		return groupKeys;
	}

}
