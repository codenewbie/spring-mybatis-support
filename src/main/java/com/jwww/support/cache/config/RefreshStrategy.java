package com.jwww.support.cache.config;

/**
 * 刷新策略
 * @author jwww
 * @date 2015年6月13日
 * @description <br>
 * Copyright (c) 2015, vakinge@gmail.com.
 */
public enum RefreshStrategy {
   GROUP,SELF
}
