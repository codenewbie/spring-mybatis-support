package com.jwww.support.cache.config;

/**
 * 数据库操作类型
 * @author jwww
 * @date 2015年6月13日
 * @description <br>
 * Copyright (c) 2015, vakinge@gmail.com.
 */
public enum ActionType {
  INSERT,DELETE,UPDATE,SELECT
}
