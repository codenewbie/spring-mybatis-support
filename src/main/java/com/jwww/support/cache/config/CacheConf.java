package com.jwww.support.cache.config;

import org.apache.commons.lang.StringUtils;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * 缓存配置
 * @author jwww
 * @date 2015年6月13日
 * @description <br>
 * Copyright (c) 2015, vakinge@gmail.com.
 */
public class CacheConf {

	private String groupName;
	
	private String key;
	
	private int timeout;
	
	private ActionType actionType;
	
	private RefreshStrategy refreshStrategy;
	
	private Object[] params;
	
	private Class<?>[] paramTypes;
	
	private List<Object> keyParams;
	
	private String referencesBy;
	
	private String cacheKey;
	
	

	public CacheConf(CacheAction cacheAction) {
		this.groupName = cacheAction.group();
		this.key = cacheAction.key();
		this.actionType = cacheAction.actionType();
		this.refreshStrategy = cacheAction.refreshStrategy();
		this.timeout = cacheAction.timeout();
		this.referencesBy = cacheAction.referencesBy();
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public ActionType getActionType() {
		return actionType;
	}

	public void setActionType(ActionType actionType) {
		this.actionType = actionType;
	}

	public RefreshStrategy getRefreshStrategy() {
		return refreshStrategy;
	}

	public void setRefreshStrategy(RefreshStrategy refreshStrategy) {
		this.refreshStrategy = refreshStrategy;
	}

	public Object[] getParams() {
		return params;
	}

	public void setParams(Object[] params) {
		this.params = params;
	}

	public Class<?>[] getParamTypes() {
		return paramTypes;
	}

	public void setParamTypes(Class<?>[] paramTypes) {
		this.paramTypes = paramTypes;
	}

	public List<Object> getKeyParams() {
		if(keyParams == null)keyParams = new ArrayList<Object>();
		return keyParams;
	}

	public void setKeyParams(List<Object> keyParams) {
		this.keyParams = keyParams;
	}
	
	
	
	public int getTimeout() {
		return timeout;
	}

	public void setTimeout(int timeout) {
		this.timeout = timeout;
	}
	

	public String getReferencesBy() {
		return referencesBy;
	}

	public void setReferencesBy(String referencesBy) {
		this.referencesBy = referencesBy;
	}

	public String getCacheKey(){
		if(cacheKey != null || StringUtils.isBlank(key))return cacheKey;
		StringBuffer sb = new StringBuffer(groupName).append(".");
		
		
		if(keyParams != null){
			List<Object> keyValues = new ArrayList<>();
			for (int i = 0; i < keyParams.size(); i++) {
				if(keyParams.get(i) != null){
					keyValues.add(params[i]);
				}
			}
			key = MessageFormat.format(key, keyValues.toArray());
		}
		sb.append(key);
		return cacheKey = sb.toString();
	}
	
	public static void main(String[] args) {
		
		System.out.println(MessageFormat.format("wo {0}-{1}", "qq"));
	}
	
}
