package com.jwww.support.cache.config;

import java.lang.annotation.*;

/**
 * @author jwww
 * @date 2015年6月13日
 * @description <br>
 * Copyright (c) 2015, vakinge@gmail.com.
 */
@Target({ ElementType.METHOD, ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Documented
public @interface CacheAction {
	
	/**所属组*/
	String group();
	
	String key() default "";

	int timeout() default 60 * 24;
	
	/**数据库操作类型*/
	ActionType actionType();
	
	/**更新策略*/
	RefreshStrategy refreshStrategy() default RefreshStrategy.GROUP;
	
	/**关联更新的组（多个用“,”隔开）*/
	String referencesBy() default "";
}
