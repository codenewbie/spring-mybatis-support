package com.jwww.support.cache;

import java.util.Collection;

public interface CacheProvider {

	/**
	 * 
	 * @param key
	 * @param value
	 * @param timeout 过期时间（单位：分钟，-1代表永不过期）
	 * @return
	 */
    public boolean set(String key, Object value, int timeout);  
	
	<T> T get(String key);
	
	boolean remove(String key);
	
	boolean remove(Collection<String> keys);
	
	boolean exists(String key);
}
