package com.jwww.support.cache.redis;

import com.jwww.support.cache.CacheProvider;
import org.springframework.data.redis.core.RedisTemplate;

import java.util.Collection;

/**
 * redis缓存实现
 * @author jwww
 * @date 2015年6月13日
 * @description <br>
 * Copyright (c) 2015, vakinge@gmail.com.
 */
public class RedisCacheProvider implements CacheProvider {
	
	private RedisTemplate<String,Object> redisTemplate;  
	

	public void setRedisTemplate(RedisTemplate<String, Object> redisTemplate) {
		this.redisTemplate = redisTemplate;
	}

	@Override
	public boolean set(String key, Object value, int timeout) {
		redisTemplate.opsForValue().set(key, value);
		return true;
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> T get(String key) {
		return (T) redisTemplate.opsForValue().get(key);  
	}

	@Override
	public boolean remove(String key) {
		redisTemplate.delete(key);
		return true;
	}

	@Override
	public boolean exists(String key) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean remove(Collection<String> keys) {
		redisTemplate.delete(keys);
		return true;
	}
	
	

}
