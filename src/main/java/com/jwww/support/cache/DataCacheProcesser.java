package com.jwww.support.cache;

import com.jwww.support.cache.config.ActionType;
import com.jwww.support.cache.config.CacheAction;
import com.jwww.support.cache.config.CacheConf;
import com.jwww.support.cache.config.RefreshStrategy;
import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


/**
 * 缓存拦截器
 * @author jwww
 * @date 2015年6月12日上午11:13:35
 * @description <br>
 * Copyright (c) 2015, vakinge@gmail.com.
 */
public class DataCacheProcesser  implements MethodInterceptor{
	
	protected static final Logger logger = LoggerFactory.getLogger(DataCacheProcesser.class);

	//处理的缓存组
	private List<String> processGroupNames;
	
	public void setProcessGroupNames(List<String> processGroupNames) {
		this.processGroupNames = processGroupNames;
	}


	@Override
	public Object invoke(MethodInvocation invocation) throws Throwable {
		Object target = invocation.getThis();
		Method targetMethod = invocation.getMethod();
		Class<?>[] targetMethodParamTypes = targetMethod.getParameterTypes();
		Method method = target.getClass().getMethod(targetMethod.getName(),
				targetMethodParamTypes);
		
		Object[] args = invocation.getArguments();

		if (method.isAnnotationPresent(CacheAction.class)) {
			CacheAction cacheAction = method.getAnnotation(CacheAction.class);
			CacheConf cacheConf = new CacheConf(cacheAction);
			cacheConf.setParamTypes(targetMethodParamTypes);
			
			Annotation[][] annotations = method.getParameterAnnotations();
	        if(annotations != null){
	        	for (int i = 0; i < annotations.length; i++) {
	        		Annotation[] aa = annotations[i];
	        		if(aa.length == 0){
	        			cacheConf.getKeyParams().add(null);
	        			continue;
	        		}
					if(aa[0].toString().contains("com.jwww.support.cache.config.CacheKey")){
						cacheConf.getKeyParams().add(aa[0].toString());
					}
				}
	        }
	        cacheConf.setParams(args);
	        return processDataWithCache(invocation, cacheConf);
		}
		return invocation.proceed(); 
	}

	private Object processDataWithCache(MethodInvocation invocation,CacheConf cacheConf) throws Throwable{
        try {
        	String cacheKey = cacheConf.getCacheKey();
        	Object result = null;
        	if(cacheConf.getActionType() == ActionType.SELECT){
        		if(cacheKey != null){
        			result = CacheKit.get(cacheKey);
        		}else{
        			logger.warn("方法["+invocation.getMethod()+"]未定义缓存KEY，忽略从缓存读取");
        		}
				
        		if(result == null){
        			result = invocation.proceed();
        			if(result != null && cacheKey != null){
        				CacheKit.put(cacheConf.getGroupName(),cacheKey, result,cacheConf.getTimeout());
        			}
        		}
        	}else{
        		result = invocation.proceed();
        		//
        		cascadeGroupUpdate(cacheConf,result);
        		if(cacheKey != null){
        			if(cacheConf.getActionType() == ActionType.DELETE){
                		CacheKit.remove(cacheConf.getGroupName(),cacheKey);
                	}else if(cacheConf.getActionType() == ActionType.INSERT || cacheConf.getActionType() == ActionType.UPDATE){
                		CacheKit.put(cacheConf.getGroupName(),cacheKey, result,cacheConf.getTimeout());
                	}
        		}
        	}
        	//
        	return result; 
		} catch (Exception e) {
			logger.error("数据缓存错误", e);
			return invocation.proceed();
		}
		
	}
	
	
	/**
	 * 级联组更新
	 * @param cacheConf
	 * @param result
	 */
	private void cascadeGroupUpdate(CacheConf cacheConf,Object result){

		List<String> cascadeGroups = new ArrayList<String>();
		
		//关联更新组
		if(StringUtils.isNotBlank(cacheConf.getReferencesBy())){
			cascadeGroups.addAll(Arrays.asList(cacheConf.getReferencesBy().split(",")));
		}
		
		if(cacheConf.getRefreshStrategy() == RefreshStrategy.GROUP){
			if(!cascadeGroups.contains(cacheConf.getGroupName())){
				cascadeGroups.add(cacheConf.getGroupName());
			}
		}
		if(logger.isDebugEnabled()){
    		logger.debug(String.format("级联更新缓存组："+Arrays.toString(cascadeGroups.toArray())));
    	}
		//删除关联更新缓存组
		CacheKit.removeGroup(cascadeGroups);
		
		String cacheKey = cacheConf.getCacheKey();
		if(cacheKey == null)return;
		String[] parts = cacheKey.split("\\.");
		
		boolean isDelete = cacheConf.getActionType() == ActionType.DELETE;
		for (String part : parts) {
			if(processGroupNames.contains(part) && !cascadeGroups.contains(part)){
				if(isDelete){
					CacheKit.removeGroupKey(part, cacheKey);
				}else{
					CacheKit.addGroupKey(part, cacheKey);
				}
			}
		}

	}

}
