/**
 * 
 */
package com.jwww.support.exception;

/** 
 * @author jwww
 * @date 2015年6月24日下午1:47:12
 * @description <br>
 * Copyright (c) 2015, vakinge@gmail.com.
 */
public class BaseServiceException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	
	private String errorCode;
	

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public BaseServiceException(String errorCode) {
		super();
		this.errorCode = errorCode;
	}
	
	public BaseServiceException(String errorCode,String defaultMessage) {
		super(defaultMessage);
		this.errorCode = errorCode;
	}

}
