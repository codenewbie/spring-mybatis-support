/**
 * 
 */
package com.jwww.support.mybatis;

import java.util.regex.Pattern;

import com.jwww.support.exception.BaseServiceException;

/**
 * @author jwww
 * @date 2015年7月15日上午9:56:29
 * @description <br>
 *              Copyright (c) 2015, vakinge@gmail.com.
 */
public class SqlChecker {

	/**
	 * 
	 */
	private static final  Pattern badPattern = Pattern.compile(".*([';=]+|(--)+|(select|update|and|or|delete|insert|trancate|char|into|substr|ascii|declare|exec|count|master|drop|execute)\\s+).*");

	public static String paramsSafeFilter(String param) {
		if(badPattern.matcher(param).matches())throw new BaseServiceException("SQL","查询参数错误["+param+"]");
		return param;
	}
	
	public static void main(String[] args) {
		System.out.println(paramsSafeFilter("select from"));
	}
}
