/**
 * 
 */
package com.jwww.support.mybatis.beans;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/** 
 * @author jwww
 * @date 2015年6月11日下午4:09:25
 * @description <br>
 * Copyright (c) 2015, vakinge@gmail.com.
 */
public class KVdata {

	private String k;
	private Object v;
	
	public KVdata() {}
	
	public KVdata(String k, Object v) {
		super();
		this.k = k;
		this.v = v;
	}

	public String getK() {
		return k;
	}
	public void setK(String k) {
		this.k = k;
	}
	public Object getV() {
		return v;
	}
	public void setV(Object v) {
		this.v = v;
	}
	
	
	public static Map<String,String> toIntMap(List<KVdata> datas){
		Map<String,String> map = new HashMap<String,String>();
		if(datas == null)return map;
		int total = 0;
        for (KVdata d : datas) {
        	if(d.getV() == null)continue;
        	total+=Integer.parseInt(d.getV().toString());
        	String val;
        	if(map.containsKey(d.getK())){
        		val = (Integer.parseInt(map.get(d.getK())) + Integer.parseInt(d.getV().toString())) + "";
        	}else{
        		val = d.getV().toString();
        	}
        	map.put(d.getK(), val);
		}
        map.put("total", total+"");
		return map;
	}
	
	public static void main(String[] args) {}
}
