package com.jwww.support.mybatis.beans;

import java.io.Serializable;

public class PageRequest implements Serializable {

	private static final long serialVersionUID = 1L;

	public PageRequest() {}

	public PageRequest(int pageNo, int pageSize, String sortFields) {
		super();
		this.pageNo = pageNo;
		this.pageSize = pageSize;
		this.sortFields = sortFields;
	}

	private int pageNo;
	
	private int pageSize;
	
	//排序字段（eg. typeId desc ,id asc）这里的字段是：映射后的实体字段
	private String sortFields;

	public int getPageNo() {
		return pageNo;
	}

	public void setPageNo(int pageNo) {
		this.pageNo = pageNo;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public String getSortFields() {
		return sortFields;
	}

	public void setSortFields(String sortFields) {
		this.sortFields = sortFields;
	}
	
	
}
