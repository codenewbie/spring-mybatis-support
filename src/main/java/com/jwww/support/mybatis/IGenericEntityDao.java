package com.jwww.support.mybatis;

import com.jwww.support.mybatis.query.NativeQuery;
import com.jwww.support.mybatis.query.EntityQuery;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;


/**
 * mybatis 通用查询接口
 * @author jwww
 * @date 2015年4月21日上午11:58:00
 * @description <br>
 * Copyright (c) 2015, vakinge@gmail.com.
 */
public interface IGenericEntityDao<T, ID extends Serializable> {

    /**
     * 保存（持久化）对象
     * @param ob 要持久化的对象
     */
	public void insert(T ob);

	
	/**
	  * 根据map更新实体(此方法避免一些不必要字段的更新)
	  * @param datas
	*/
	public void update(Map<String, Object> datas);
	
	
	public void updateByQuery(NativeQuery query);

    /**
     * 获取指定的唯一标识符对应的持久化对象
     *
     * @param id 指定的唯一标识符
     * @return 指定的唯一标识符对应的持久化对象，如果没有对应的持久化对象，则返回null。
     */
	public T getById(ID id);
	
	/**
	 * 根据参入主键批量查询
	 * @param ids
	 * @return
	 */
	public List<T> getListByIds(List<String> ids);

    /**
     * 删除指定的唯一标识符对应的持久化对象
     *
     * @param id 指定的唯一标识符
	 * @return 删除的对象数量
     */
	public Integer deleteById(ID id);

    /**
     * 删除指定的唯一标识符数组对应的持久化对象
     *
     * @param ids 指定的唯一标识符数组
	 * @return 删除的对象数量
     */
	public Integer deleteByIds(List<String> ids);
	
	/**
	 * 按查询条件删除
	 * @param query
	 */
	public void deleteByQuery(NativeQuery query);
	
	/**
	 * 获取满足查询参数条件的数据总数
	 * 
	 * @param params 查询参数
	 * @return 数据总数
	 */
	public Integer getCount(EntityQuery params);

	/**
	 * 按条件查询列表
	 * 
	 * @param params
	 */
	public List<T> findLists(EntityQuery params);
	
	/**
	 * 分页查询
	 * 
	 * @param params 包含查询条件、start、limit，sortAs字段
	 */
	public List<T> findListByPage(EntityQuery params);
	
	/**
	 * 通过查询条件查询所有记录
	 * @param query  查询条件（原生SQL）
	 * @return
	 */
	public List<T> findListByQuery(NativeQuery query);

	/**
	 * 通过查询条件查询单一记录
	 * @param query 查询条件（原生SQL）
	 * @return
	 */
	public T findOneByQuery(NativeQuery query);
	
	
	/**
	 * 数字相关统计查询（支持：AVG,COUNT,MIN,MAX,SUM）<BR>
	 *  用法：new GenericQuery().sum('amount');
	 * @param query  （原生SQL）
	 * @return
	 */
	public BigDecimal statisticsQuery(NativeQuery query);
	
}
